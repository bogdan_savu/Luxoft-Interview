﻿namespace SumOfMultiple
{
    /// <summary>
    /// Sum of Multiple class.
    /// </summary>
    public static class SumOfMultipleFunc
    {

        /// <summary>
        /// Gets the sum of numbers by a limit.
        /// </summary>
        /// <param name="limit">The upper limit.</param>
        /// <returns>The sum of numbers as long.</returns>
        public static long GetSumOfNumbers(int limit)
        {
            if (limit <= 3) return 0; //case when no number is eligible.
            var response = 0;
            for (int index = 0; index < limit; index++)
            {
                if (index % 3 == 0 || index % 5 == 0)
                    response += index;
            }

            return response;
        }

    }
}
