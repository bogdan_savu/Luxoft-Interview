﻿using System;
using System.Threading;
using DivisionTesting;
using SequenceAnalysis;
using SumOfMultiple;

namespace Runner
{
    /// <summary>
    /// Class used for running/testing Assignments.
    /// </summary>
    class Runner
    {
        /// <summary>
        /// Main method called at startup.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            Console.WriteLine("Assignment 1. Please select an option:");
            Console.WriteLine("Enter M for SumOfMultiple problem or A for SequenceAnalysis problem:");
            var input = Console.ReadLine(); //get needed input from user

            try
            {
                switch (input.ToUpper())
                {
                    case "M":
                        Console.WriteLine("Please provide a natural number as upper limit:");
                        var isInteger = int.TryParse(Console.ReadLine(), out int limit);
                        if (!isInteger)
                        {
                            Console.WriteLine("Your option was not valid. Please rerun the program!");
                            break;
                        }
                        var sumResult = SumOfMultipleFunc.GetSumOfNumbers(limit);
                        Console.WriteLine($"The sum result is: {sumResult}");
                        Thread.Sleep(2000);
                        break;
                    case "A":
                        Console.WriteLine("Please enter a valid string:");
                        var inputString = Console.ReadLine();
                        if (string.IsNullOrEmpty(inputString))
                        {
                            Console.WriteLine("Your option was not valid. Please rerun the program!");
                            break;
                        }
                        var upperLetters = SequenceAnalysisFunc.GetUpperCaseAlpabetically(inputString);
                        Console.WriteLine($"The upper letters ordered alphabetically are: {upperLetters}");
                        Thread.Sleep(2000);
                        break;
                    default:
                        Console.WriteLine("Your option was not valid. Please rerun the program!");
                        Thread.Sleep(2000);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"There was an unexpected error: {ex.Message}"); //uncaught case
            }

            //uncomment code to test Assignment2

            //Console.WriteLine("Please write 2 numbers to check if division is possible:");
            //var isXInteger = int.TryParse(Console.ReadLine(), out int X);
            //var isYInteger = int.TryParse(Console.ReadLine(), out int Y);
            //if (!isXInteger || !isYInteger)
            //    Console.WriteLine("Your option was not valid. Please rerun the program!");
            //else
            //{
            //    int divisionResponse = DivisionTestingFunc.IsXDivisibleByY(X, Y);
            //    Console.WriteLine($"the division response: {divisionResponse}");
            //    Thread.Sleep(2000);
            //}

        }
    }
}
