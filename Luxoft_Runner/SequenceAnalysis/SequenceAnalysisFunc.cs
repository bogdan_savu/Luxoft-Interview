﻿using System.Linq;

namespace SequenceAnalysis
{
    /// <summary>
    /// The Sequence analysis class.
    /// </summary>
    public static class SequenceAnalysisFunc
    {

        /// <summary>
        /// Get the upper case letters in alphabetical order.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <returns>Upper case letters in alphabetical order</returns>
        public static string GetUpperCaseAlpabetically(string input)
        {
            var words = input.Split(' ');
            var upperCaseWords = string.Concat(words.Where(word => word.All(char.IsUpper))); //get upper case words in a string
            
            return new string (upperCaseWords.OrderBy(c => c).ToArray()); // new string faster than string.Concat
        }

    }
}
