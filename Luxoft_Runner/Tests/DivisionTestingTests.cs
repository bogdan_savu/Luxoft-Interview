using DivisionTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Tests
{
    /// <summary>
    /// DivisionTestingTests class.
    /// </summary>
    [TestClass]
    public class DivisionTestingTests
    {

        readonly Random rand = new Random(); //random number

        [TestMethod]
        public void IsXDivisibleByY_ReturnsTrue()
        {          
            var result = DivisionTestingFunc.IsXDivisibleByY(rand.Next(), 1);

            Assert.IsTrue(Convert.ToBoolean(result));
        }

        [TestMethod]
        public void IsXDivisibleByY_ReturnsFalse()
        {
            var result = DivisionTestingFunc.IsXDivisibleByY(rand.Next(), 0); //division by 0

            Assert.IsFalse(Convert.ToBoolean(result));
        }
    }
}
