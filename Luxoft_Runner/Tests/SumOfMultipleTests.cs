﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SumOfMultiple;

namespace Tests
{
    /// <summary>
    /// SumOfMultipleTests class.
    /// </summary>
    [TestClass]
    public class SumOfMultipleTests
    {
        [TestMethod]
        [DataRow(3)]
        [DataRow(-247)]
        [DataRow(0)]
        public void GetSumOfNumbers_LowerLimit(int limit)
        {
            var result = SumOfMultipleFunc.GetSumOfNumbers(limit);

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        [DataRow(4)]
        [DataRow(247)]
        public void GetSumOfNumbers_Success(int limit)
        {
            var result = SumOfMultipleFunc.GetSumOfNumbers(limit);

            Assert.IsTrue(result > 0);
        }

    }
}
