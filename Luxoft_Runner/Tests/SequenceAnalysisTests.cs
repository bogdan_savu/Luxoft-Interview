﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SequenceAnalysis;

namespace Tests
{
    /// <summary>
    /// SequenceAnalysisTests class.
    /// </summary>
    [TestClass]
    public class SequenceAnalysisTests
    {

        [TestMethod]
        [DataRow("This IS a STRING", "GIINRSST")]
        [DataRow("This iS a STRINg", "")]
        [DataRow("ABCD abcd", "ABCD")]
        [DataRow("", "")]
        public void GetUpperCaseAlpabetically_Success(string inputString, string outputString)
        {
            var result = SequenceAnalysisFunc.GetUpperCaseAlpabetically(inputString);

            Assert.AreEqual(result, outputString);
        }
    }
}
