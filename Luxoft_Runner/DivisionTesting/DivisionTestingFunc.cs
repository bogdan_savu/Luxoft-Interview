﻿using System;

namespace DivisionTesting
{
    /// <summary>
    /// The division Testing class.
    /// </summary>
    public static class DivisionTestingFunc
    {

        /// <summary>
        /// The divisible check function for 2 integers;
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <returns></returns>
        public static int IsXDivisibleByY(int X, int Y)
        {
            var remainder = !Convert.ToBoolean(X % Convert.ToDouble(Y)); // convert one var to double to skip DivideByZeroException.
            return Convert.ToInt32(remainder);
        }
    }
}
